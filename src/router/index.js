import Vue from 'vue'
import VueRouter from 'vue-router'
import VueSweetalert2 from 'vue-sweetalert2'
import vSelect from 'vue-select'

Vue.use(VueSweetalert2);
Vue.component('v-select', vSelect)

Vue.use(VueRouter)
const routes = [{
        path: '/',
        name: 'Home',
        component: () => import('../views/Home.vue')
    },
    // management user
    {
        path: '/management-user',
        name: 'ManagementUser',
        component: () => import('../views/ManagementUser/ListUser.vue')
    },
    {
        path: '/add-user',
        name: 'AddUser',
        component: () => import('../views/ManagementUser/AddUser.vue')
    },
    {
        path: '/edit-user/:id',
        name: 'EditUser',
        component: () => import('../views/ManagementUser/EditUser.vue')
    },
    {
        path: '/user-matrix/:id',
        name: 'UserMatrix',
        component: () => import('../views/ManagementUser/UserMatrix.vue')
    },
   
    {
        path: '/login',
        name: 'Login',
        component: () => import('../views/Login.vue')
    }

]

const router = new VueRouter({
    routes
})

export default router